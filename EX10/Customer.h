#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string name);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set
	std::string getName() const;
	void setName(std::string name);
	std::set<Item> getItems() const;

private:
	std::string _name;
	std::set<Item> _items;


};
