#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string name, std::string serialNumber, double unitPrice, int count);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.
	std::string getName() const;
	std::string getSerialNumber() const;
	int getCount() const;
	double getUnitPrice() const;
	void setName(std::string name);
	void setSerialNumber(std::string serialNumber);
	void setCount(int count);
	void setUnitPrice(int unitPrice);
	//get and set functions

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};