#include"Customer.h"
#include<map>

using std::cout;
using std::endl;
using std::cin;

int printMainMenu();

int main()
{

	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = 
	{
		Item("Milk","00001",5.3,1),
		Item("Cookies","00002",12.6,1),
		Item("bread","00003",8.9,1),
		Item("chocolate","00004",7.0,1),
		Item("cheese","00005",15.3,1),
		Item("rice","00006",6.2,1),
		Item("fish", "00008", 31.65,1),
		Item("chicken","00007",25.99,1),
		Item("cucumber","00009",1.21,1),
		Item("tomato","00010",2.32,1)
	};
	int choice = 0;
	std::string name;
	int i = 0;
	double max = 0;
	//declaring vars
	while (true)
	{
		choice = printMainMenu();
		if (choice == 1)
		{
			cout << "Enter customer's name: ";
			cin >> name;
			//getting name from user
			if (abcCustomers.find(name) == abcCustomers.end())
			{
				system("CLS");
				abcCustomers.insert(std::pair<std::string, Customer>(name, Customer(name)));
				while (choice != 0)
				{
					cout << "The items you can buy are: (0 to exit)" << endl;
					for (i = 0; i < 10; i++)
					{
						cout << i + 1 << ". " << itemList[i].getName() << ", Price: " << itemList[i].getUnitPrice() << endl;
					}
					cout << "What item would you like to buy? Input: ";
					cin >> choice;
					if (choice <= 10 && choice > 0)
					{
						abcCustomers[name].addItem(itemList[choice - 1]);
					}
					else if(choice != 0)
					{
						cout << "INVALID CHOICE!" << endl;
						system("PAUSE");
					}
					system("CLS");
				}
			}
			else
			{
				cout << "Error! customer already exists!" << endl;
				system("Pause");
				system("CLS");
			}
		}
		else if (choice == 2)
		{
			cout << "Enter customer's name: ";
			cin >> name;
			//getting name from user
			if (abcCustomers.find(name) == abcCustomers.end())
			{
				cout << "Error! customer doesn't exists!" << endl;
				system("Pause");
				system("CLS");
			}
			else
			{
				cout << "1. Add items" << endl;
				cout << "2. Remove items" << endl;
				cout << "3. Back to menu" << endl;
				cin >> choice;
				if (choice == 1)
				{
					system("CLS");
					while (choice != 0)
					{
						cout << "The items you can buy are: (0 to exit)" << endl;
						for (i = 0; i < 10; i++)
						{
							cout << i + 1 << ". " << itemList[i].getName() << ", Price: " << itemList[i].getUnitPrice() << endl;
						}
						cout << "What item would you like to buy? Input: ";
						cin >> choice;
						if (choice <= 10 && choice > 0)
						{
							abcCustomers[name].addItem(itemList[choice - 1]);
						}
						else if (choice != 0)
						{
							cout << "INVALID CHOICE!" << endl;
							system("PAUSE");
						}
						system("CLS");
					}
				}
				else if (choice == 2)
				{
					system("CLS");
					while (choice != 0)
					{
						std::set<Item> items = abcCustomers[name].getItems();
						std::set<Item>::iterator curr = items.begin();
						i = 0;
						for (std::set<Item>::iterator it = items.begin(); it != items.end(); ++it)
						{
							i++;
							cout << i << ". " << it->getName() << ": " << it->getCount() << endl;
						}
						cout << "What item would you like to remove? Input: ";
						cin >> choice;
						if (choice <= 10 && choice > 0)
						{
							std::advance(curr, choice - 1);
							abcCustomers[name].removeItem(*curr);
						}
						else if (choice != 0)
						{
							cout << "INVALID CHOICE!" << endl;
							system("PAUSE");
						}
						system("CLS");
					}
				}
				else if (choice == 3)
				{
					system("CLS");
				}
				else
				{
					cout << "INVALID CHOICE!" << endl;
					system("PAUSE");
					system("CLS");
				}
				system("CLS");
			}
		}
		else if (choice == 3)
		{
			if(abcCustomers.empty())
			{
				cout << "No customers yet!" << endl;
			}
			else
			{
				for (std::map<std::string, Customer>::iterator it = abcCustomers.begin(); it != abcCustomers.end(); ++it)
				{
					if (it->second.totalSum() > max)
					{
						max = it->second.totalSum();
						name = it->second.getName();
					}
				}
				cout << "The customer that paid the most is " << name << " with the total of " << max << " shekels." << endl;
			}
			system("PAUSE");
			system("CLS");
		}
		else
		{
			_exit(1);
		}
	}
	return 0;
}


/*
	Function prints the main menu
	Input:
		None
	Output:
		Choice
*/
int printMainMenu()
{
	int choice = 0;
	//declaring vars
	do
	{
		cout << "Welcome to MagshiMart!" << endl;
		cout << "1. to sign as customer and buy items" << endl;
		cout << "2. to update existing customer's items" << endl;
		cout << "3. to print the customer who pays the most" << endl;
		cout << "4. to exit" << endl;
		cin >> choice;
		system("CLS");
	} while (choice < 1 || choice > 4);
	return choice;
}