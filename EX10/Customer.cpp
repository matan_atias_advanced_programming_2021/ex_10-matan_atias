#include "Customer.h"

/*
	Constructor
	Input:
		name
	Output:
		None
*/
Customer::Customer(std::string name)
{
	this->_name = name;
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Customer::Customer()
{

}


/*
	returns the total sum for payment
	Input:
		None
	Output:
		Total sum for payment
*/
double Customer::totalSum() const
{
	double sum = 0;
	//declaring vars
	for (std::set<Item>::iterator it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		sum += it->totalPrice();
	}
	return sum;
}


/*
	add item to the set
	Input:
		Item to add
	Output:
		None
*/
void Customer::addItem(Item item)
{
	if (this->_items.find(item) != this->_items.end())
	{
		auto currItem = this->_items.find(item);
		Item newItem(item.getName(), item.getSerialNumber(), item.getUnitPrice(), currItem->getCount() + 1);
		this->_items.erase(this->_items.find(item));
		this->_items.insert(newItem);
	}
	else
	{
		this->_items.insert(item);
	}
}


/*
	remove item from the set
	Input:
		item to remove
	Output:
		None
*/
void Customer::removeItem(Item item)
{
	if (item.getCount() != 1)
	{
		Item newItem(item.getName(), item.getSerialNumber(), item.getUnitPrice(), item.getCount() - 1);
		this->_items.erase(this->_items.find(item));
		this->_items.insert(newItem);
	}
	else
	{
		this->_items.erase(item);
	}
}


/*
	Function returns the name of the customer
	Input:
		None
	Output:
		Name
*/
std::string Customer::getName() const
{
	return this->_name;
}


/*
	Function sets the name of the customer
	Input:
		name
	Output:
		None
*/
void Customer::setName(std::string name)
{
	this->_name = name;
}


/*
	Function returns the items set of a customer
	Input:
		None
	Output:
		set of items
*/
std::set<Item> Customer::getItems() const
{
	return this->_items;
}