#include "Item.h"

/*
	Constructor
	Input:
		name, serialNumber, unitPrice
	Output:
		None
*/
Item::Item(std::string name, std::string serialNumber, double unitPrice, int count)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = count;
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Item::~Item()
{

}


/*
	returns _count*_unitPrice
	Input:
		None
	Output:
		totalPrice
*/
double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}


/*
	compares the _serialNumber of those items.
	Input:
		other item
	Output:
		true/false
*/
bool Item::operator <(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}


/*
	compares the _serialNumber of those items.
	Input:
		other item
	Output:
		true/false
*/
bool Item::operator >(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}


/*
	compares the _serialNumber of those items.
	Input:
		other item
	Output:
		true/false
*/
bool Item::operator ==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}


/*
	Getter for name
	Input:
		None
	Output:
		Name
*/
std::string Item::getName() const
{
	return this->_name;
}


/*
	Getter for serial number
	Input:
		None
	Output:
		Serial number
*/
std::string Item::getSerialNumber() const
{
	return this->_serialNumber;
}


/*
	Getter for count
	Input:
		None
	Output:
		Count
*/
int Item::getCount() const
{
	return this->_count;
}


/*
	Getter for unit price
	Input:
		None
	Output:
		Unit price
*/
double Item::getUnitPrice() const
{
	return this->_unitPrice;
}


/*
	Setter for name
	Input:
		Name
	Output:
		None
*/
void Item::setName(std::string name)
{
	this->_name = name;
}


/*
	Setter for serial number
	Input:
		Serial number
	Output:
		None
*/
void Item::setSerialNumber(std::string serialNumber)
{
	this->_serialNumber = serialNumber;
}


/*
	Setter for count
	Input:
		Count
	Output:
		None
*/
void Item::setCount(int count)
{
	this->_count = count;
}


/*
	Setter for unit price
	Input:
		UnitPrice
	Output:
		None
*/
void Item::setUnitPrice(int unitPrice)
{
	this->_unitPrice = unitPrice;
}